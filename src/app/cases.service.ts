import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentChangeAction} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import firebase from 'firebase';
import {Case} from './interfaces/case';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CasesService {

  constructor(private db: AngularFirestore, private http: HttpClient) {
  }

  private predictionUrl = 'https://8w9qbc7mlj.execute-api.us-east-1.amazonaws.com/beta';

  casesCollection: AngularFirestoreCollection;
  userCollection: AngularFirestoreCollection = this.db.collection('users');

  public predictCase(c: Case): Observable<object> {
    console.log(this.getCaseDataString(c));
    const json = {data: this.getCaseDataString(c)};
    const body = JSON.stringify(json);
    console.log(body);
    return this.http.post(this.predictionUrl, body);
  }

  public getNews(): Observable<object> {
    return this.http.get('https://newsapi.org/v2/everything?qInTitle=suicide&apiKey=33ba70d6367648b49a76910dfad62ad4&sortBy=publishedAt');
  }


  public getCases(userId: string): Observable<DocumentChangeAction<firebase.firestore.DocumentData>[]> {
    this.casesCollection = this.db.collection(`users/${userId}/cases`);
    return this.casesCollection.snapshotChanges().pipe(map(
      collection => collection.map(
        document => {
          const data = document;
          return data;
        })));
  }


  public addCase(userId: string, c: Case): void {
    this.userCollection.doc(userId).collection('cases').add(c).then(r => {
      console.log(r);
    });
  }

  public deleteCase(userId: string, id: string): void {
    this.db.doc(`users/${userId}/cases/${id}`).delete().then(r => {
      console.log(r);
    });
  }

  public updateCase(userId: string, c: Case): void {
    this.db.doc(`users/${userId}/cases/${c.id}`).update({
      police: c.police,
      sex: c.sex,
      age: c.age,
      month: c.month,
      race: c.race,
      place: c.place,
      education: c.education,
      suicide: c.suicide
    }).then(r => {
      console.log(r);
    });
  }

  private getCaseDataString(c: Case): string {
    return '2012'.concat(',').concat(c.month).concat(',').concat((+c.police).toString())
      .concat(',').concat(c.sex === 'Male' ? '1' : '2').concat(',').concat(c.age.toString())
      .concat(',').concat(this.getRaceNumber(c.race)).concat(',').concat(this.getPlaceNumber(c.place))
      .concat(',').concat(this.getEducationNum(c.education));
  }

  private getRaceNumber(race: string): '1' | '2' | '3' | '4' | '5' {
    switch (race) {
      case 'Asian':
        return '1';
      case 'White':
        return '2';
      case 'Native American':
        return '3';
      case 'Black':
        return '4';
      case 'Hispanic':
        return '5';
    }
  }

  private getPlaceNumber(place: string): '1' | '2' | '3' | '4' | '5' | '6' {
    switch (place) {
      case 'Home':
        return '1';
      case 'Street':
        return '2';
      case 'Other specified':
        return '3';
      case 'Other unspecified':
        return '4';
      case 'Trade / service area':
        return '5';
      case 'School / institution':
        return '6';
    }
  }

  private getEducationNum(education: string): '1' | '2' | '3' | '4' | '5' {
    switch (education) {
      case 'less than high school':
        return '1';
      case 'High School':
        return '2';
      case 'College':
        return '3';
      case 'At least graduated from College':
        return '4';
      case 'N/A':
        return '5';
    }
  }
}

