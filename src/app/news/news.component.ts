import {Component, OnInit} from '@angular/core';
import {CasesService} from '../cases.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  articles = [];

  constructor(public caseService: CasesService) {
  }

  ngOnInit(): void {
    this.caseService.getNews().subscribe((res: any) => {
      this.articles = res.articles;
      console.log(this.articles);
    });
  }
}
