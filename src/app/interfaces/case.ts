export interface Case {
  id?: string;
  police: boolean;
  sex: string;
  age: number;
  month: string;
  race: string;
  place: string;
  education: string;
  suicide?: string;
}
