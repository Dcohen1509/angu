import {Component, Inject, OnInit} from '@angular/core';
import {Case} from '../interfaces/case';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CasesService} from "../cases.service";

@Component({
  selector: 'app-case-form',
  templateUrl: './case-form.component.html',
  styleUrls: ['./case-form.component.css']
})
export class CaseFormComponent implements OnInit {

  title: string;

  races = ['Asian', 'White', 'Native American', 'Black', 'Hispanic'];

  genders = ['Male', 'Female'];

  places = ['Home', 'Street', 'Other specified', 'Other unspecified', 'Trade / service area', 'School / institution'];

  educations = ['less than high school', 'High School', 'College', 'At least graduated from College', 'N/A'];

  case;
  months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  constructor(private caseService: CasesService,
              public dialogRef: MatDialogRef<CaseFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { case: Case, title: string, userId: string }) {
  }

  ngOnInit(): void {
    this.case = this.data.case ? this.data.case : {police: false};
    this.title = this.data.title;
  }

  saveCase(): void {
    if (this.title === 'Add') {
      this.caseService.addCase(this.data.userId, this.case);
      this.dialogRef.close(true);
    } else {
      this.case.suicide = null;
      this.caseService.updateCase(this.data.userId, this.case);
      this.dialogRef.close(false);
    }
  }

  cancelDialog(): void {
    this.dialogRef.close(false);
  }
}
