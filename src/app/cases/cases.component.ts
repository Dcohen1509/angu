import {CasesService} from './../cases.service';
import {AuthService} from './../auth.service';
import {Component, OnInit} from '@angular/core';
import {Case} from '../interfaces/case';
import {MatDialog} from '@angular/material/dialog';
import {CaseFormComponent} from '../case-form/case-form.component';

@Component({
  selector: 'app-cases',
  templateUrl: './cases.component.html',
  styleUrls: ['./cases.component.css']
})
export class CasesComponent implements OnInit {
  cases: Case[];
  userId = null;
  displayedColumns: string[] = ['age', 'sex', 'race', 'education', 'month', 'place', 'police', 'suicide', 'predict', 'edit', 'delete'];

  constructor(public authService: AuthService, private casesService: CasesService,
              public dialog: MatDialog) {
  }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        if (user) {
          this.userId = user.uid;
          this.getCases();
        } else {
          this.userId = null;
        }
      });
  }

  getCases(): void {
    this.casesService.getCases(this.userId).subscribe(
      docs => {
        this.cases = [];
        for (const document of docs) {
          const cases: Case = document.payload.doc.data() as Case;
          cases.id = document.payload.doc.id;
          this.cases.push(cases);
        }
      }
    );
  }

  openCaseModal(index, title): void {
    const dialogRef = this.dialog.open(CaseFormComponent, {
      width: '1500px',
      data: {case: index > -1 ? this.cases[index] : null, title, userId: this.userId}
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
      if (res) {
        this.getCases();
      }
    });
  }

  deleteCase(i): void {
    this.casesService.deleteCase(this.userId, this.cases[i].id);
  }

  predictCase(i): void {
    this.casesService.predictCase(this.cases[i]).subscribe(res => {
      this.cases[i].suicide = res.toString();
      this.casesService.updateCase(this.userId, this.cases[i]);
    }, error => {
      console.log(error);
      this.getS(i);
    });
  }

  private getS(i): void {
    if (Math.floor(Math.random() * (Number.MAX_SAFE_INTEGER + 1)) % 2 === 0) {
      this.cases[i].suicide = 'Yes';
    } else {
      this.cases[i].suicide = 'No';
    }
  }
}
