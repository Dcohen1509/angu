// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA_-sy_RoZv0OV9THVZIsL9AA8jPGJ8UCg",
    authDomain: "finalproject-5e377.firebaseapp.com",
    projectId: "finalproject-5e377",
    storageBucket: "finalproject-5e377.appspot.com",
    messagingSenderId: "56486271642",
    appId: "1:56486271642:web:ec302b95c8a2a518a09d23"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
